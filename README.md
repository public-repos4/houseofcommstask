After you download or clone the repo do the following:

## Running the Angular App:
-Install NodeJS and NPM from https://nodejs.org. \
-Install the Angular CLI using the command: npm install -g @angular/cli \
-Start the app with the Angular CLI command ng serve --open this will compile the Angular app and automatically launch it in the browser on the URL http://localhost:4200.

## Running the ASP.NET Core 5 API:
-Install the .NET Core SDK from https://www.microsoft.com/net/download/core. \
-Open the project in visual studio and run it you should see the message Now listening on: http://localhost:4000

## Assumptions:
-We have unsorted numberd boarding cards and we want to order them so we can travel from a country to another until getting the final desination

## Notes
-The "Quick Sort" algorithm has been used to complete this task, because it has the minimal Big(O) (n log(n)\
-The implemented code accepted unlimited number of numberd and unsorted cards\
-The algorithm doesn't need to consider that departure / arrival are in the correct order. (It sort the cards based on the card number)\
-The HTML page is fully responsive\
-The HTML page has header, footer and and a slider to show the sorted cards