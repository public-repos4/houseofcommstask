﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApi.Models;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using System.Xml.Linq;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TripSorterController : ControllerBase
    {
        protected readonly IWebHostEnvironment _webHostEnvironment;

        public TripSorterController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }


        [HttpGet]
        public IActionResult SortCards()
        {
			DirectoryInfo dirSource = new(_webHostEnvironment.ContentRootPath);

			var doc = XDocument.Load(dirSource.GetFiles("*.xml", SearchOption.AllDirectories).ToList()[0].FullName);

            var cards = doc.Root
                .Descendants("card")
                .Select(node => new BoardingCard()
                {
                    Id = (int)node.Element("id"),
                    Description = (string)node.Element("description")
                })
                .ToList();

            return Ok(Sort(cards, 0, cards.Count -1));
        }

		private static List<BoardingCard> Sort(List<BoardingCard> cards, int leftIndex, int rightIndex)
		{
			var i = leftIndex;
			var j = rightIndex;
			var pivot = cards[leftIndex].Id;

			while (i <= j)
			{
				while (cards[i].Id < pivot) i++;

				while (cards[j].Id > pivot) j--;

				if (i <= j)
				{
					BoardingCard temp = cards[i];
					cards[i] = cards[j];
					cards[j] = temp;
					i++;
					j--;
				}
			}

			if (leftIndex < j) Sort(cards, leftIndex, j);

			if (i < rightIndex) Sort(cards, i, rightIndex);

			return cards;
		}
	}
}
