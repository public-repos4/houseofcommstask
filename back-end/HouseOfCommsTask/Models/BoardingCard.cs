using System.Xml.Serialization;

namespace WebApi.Models
{
    [XmlRoot(ElementName = "boardingCard")]
    public class BoardingCard
    {
        [XmlElement(ElementName = "id")]
        public int Id { get; set; }

        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
    }
}