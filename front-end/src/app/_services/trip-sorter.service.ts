﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import {Card} from '../_models/card';

@Injectable({ providedIn: 'root' })
export class TripSorterService {

    constructor(
        private router: Router,
        private http: HttpClient
    ) {
    }

    getAll() {
        return this.http.get<Card[]>(`${environment.apiUrl}/TripSorter`);
    }
}
