import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const tripCardsModule = () => import('./trip-cards/trip-cards.module').then(x => x.TripCardsModule);

const routes: Routes = [
    { path: '', loadChildren: tripCardsModule},

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
