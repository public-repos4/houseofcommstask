import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { TripCardsRoutingModule } from './trip-cards-routing.module';
import { LayoutComponent } from './layout.component';
import { ListComponent } from './list.component';
import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TripCardsRoutingModule,
    NgbCarouselModule
  ],
    declarations: [
        LayoutComponent,
        ListComponent,
    ]
})
export class TripCardsModule { }
