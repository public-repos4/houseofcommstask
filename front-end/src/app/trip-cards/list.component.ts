﻿import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {TripSorterService} from '../_services/trip-sorter.service';
import {Card} from '../_models/card';

@Component({
  templateUrl: 'list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  cards: Card[] = null;

  constructor(private tripSorterService: TripSorterService) {
  }

  ngOnInit() {
    this.tripSorterService.getAll()
      .pipe(first())
      .subscribe(cards => this.cards = cards);
  }
}
